Feature: Calculator - Gitlab edit
 
Calculator for adding two numbers
 
@mytag @calculator @connect
Scenario: Add two numbers
Add two numbers with the calculator
Given I have entered <First> into the calculator
And I have entered <Second> into the calculator

@calculator
Scenario: Add three numbers
Add two numbers with the calculator
Given I have entered <First> into the calculator
And I have entered <Second> into the calculator
And I have entered <Third> into the calculator