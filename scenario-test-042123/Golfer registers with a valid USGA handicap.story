Feature:
  Golfer registers with a valid USGA handicap

Scenario: Golfer registers with a valid USGA handicap
    GIVEN Golfer has signed-up for the tournament
    AND Golfer registers on the day of tournament for play
    WHEN Golf Pro requests proof of USGA handicap
    AND Golfer shows an official USGA handicap card
    THEN Golfer is assigned a tee time
